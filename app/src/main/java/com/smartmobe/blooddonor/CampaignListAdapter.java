package com.smartmobe.blooddonor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to adapt data returned from REST Api to the required format to be shown in list.
 * So in general adapters acts as a bridge between list and underlying data for the list.
 * <p>
 * Created by Ashish Kayastha on Aug 02.
 */
public class CampaignListAdapter extends RecyclerView.Adapter<CampaignListAdapter.CampaignListHolder> {

    private final LayoutInflater mInflater;

    // This is a list of campaigns returned from REST Api
    private List<Campaign> mCampaignList = new ArrayList<>();

    private OnItemClickListener mOnItemClickListener;

    public CampaignListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    // This is used to update our campaign list
    public void updateCampaignList(List<Campaign> campaignList) {
        mCampaignList = campaignList;
    }

    @Override
    public CampaignListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflate our layout which is used to show our single campaign in list
        final View view = mInflater.inflate(R.layout.list_item_campaign_layout, parent, false);
        return new CampaignListHolder(view);
    }

    @Override
    public void onBindViewHolder(CampaignListHolder holder, int position) {
        // Get campaign from list for particular position in list
        final Campaign campaign = mCampaignList.get(position);
        holder.bindData(campaign, mOnItemClickListener);

    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        mOnItemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return mCampaignList.size();
    }

    /**
     * A ViewHolder describes an item view. ViewHolder caches views defined for our layout for reuse.
     * This is useful because findViewById is expensive call which means each findViewById calls
     * requires extra resources.
     */
    static class CampaignListHolder extends RecyclerView.ViewHolder {

        public final View mRootItemView;

        // TextViews are views which are used to show texts
        public final TextView mCampaignNameText, mCampaignAddressText, mCampaignDateTimeText;

        public CampaignListHolder(View itemView) {
            super(itemView);

            // Initialize our views from xml
            mRootItemView = itemView.findViewById(R.id.root_item_view);
            mCampaignNameText = (TextView) itemView.findViewById(R.id.text_campaign_name);
            mCampaignAddressText = (TextView) itemView.findViewById(R.id.text_campaign_address);
            mCampaignDateTimeText = (TextView) itemView.findViewById(R.id.text_campaign_date_time);
        }

        public void bindData(final Campaign campaign, final OnItemClickListener onItemClickListener) {
            // Populate campaign name, address, date into textview
            mCampaignNameText.setText(campaign.getCampaignName());
            mCampaignAddressText.setText(campaign.getAddress());
            mCampaignDateTimeText.setText(campaign.getCampaignDateTime());

            /**
             * When a root view of list item is clicked trigger onItemClick method of
             * OnItemClickListener so that we can go to next activity i.e. detail activity in our case.
             * Campaign model will be passed along with onItemClick method which will be used
             * passed to detail activity from list activity.
             */
            mRootItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(campaign);
                    }
                }
            });
        }
    }
}