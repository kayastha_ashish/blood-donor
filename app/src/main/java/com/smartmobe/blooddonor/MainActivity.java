package com.smartmobe.blooddonor;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements OnItemClickListener {

    // Base url of api
    private static final String API_BASE_URL = "http://blooddonorplus.com/";

    // Log Tag
    private static final String TAG = "MainActivity";

    // Create a retrofit instance which will be used to make api calls and get campaign list
    private static final Retrofit RETROFIT = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(API_BASE_URL)
            .build();

    private CampaignListAdapter mCampaignListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set this activity content to activity_main.xml from a layout resource
        setContentView(R.layout.activity_main);

        // Initialize Toolbar and set toolbar as an app's actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Initialize RecyclerView to show campaign list
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.campaign_list);

        /**
         * RecyclerView uses Layout Manager to determine how to show our data in list.
         * There are three types of Layout Manager: Linear, Grid and Staggered
         * Using Linear, data are displayed either in vertical or horizontal orientation.
         * By default, Linear Layout Manager uses vertical orientation.
         */
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        /**
         * By default recyclerview doesn't have divider as in listview.
         * So to add divider we need to provide an item decoration which creates divider.
         * This will show a thin divider line between list items.
         */
        recyclerView.addItemDecoration(new DividerDecoration(this));

        /**
         * Set adapter in recyclerview. This gives knowledge to recyclerview how our data
         * should be shown in list
         */
        mCampaignListAdapter = new CampaignListAdapter(this);
        recyclerView.setAdapter(mCampaignListAdapter);

        /**
         * Set OnItemClickListener on adapter to handle listitem click.
         */
        mCampaignListAdapter.setOnItemClickListener(this);

        /**
         * Initialize our progressbar to be shown when we are loading campigns from server.
         * This will be hidden when campaigns list is loaded in recyclerview.
         */
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        /**
         * Create a campaign service using Retrofit instance created above.
         * This service is used to get campaign list from server.
         */
        final CampaignService campaignService = RETROFIT.create(CampaignService.class);

        /**
         * Enqueue is used to make api calls asynchronously. When the api call is completed it notifies
         * the two callback methods: onResponse and onFailure.
         * onResponse is called when api call is successful. In onResponse method we get the data returned
         * by server; in our case campaign list.
         * onFailure is called when the api call we made wasn't successful. onFailure may be called
         * due to problem in internet connection while making api calls, due to unauthorized access, etc.
         * The cause of error can be found by logging message from throwable.
         */
        campaignService.getCampaignList().enqueue(new Callback<List<Campaign>>() {
            @Override
            public void onResponse(Call<List<Campaign>> call, Response<List<Campaign>> response) {
                // Hide the progressbar when campaign list is loaded into recyclerview
                progressBar.setVisibility(View.GONE);

                /**
                 * Get campaign list from response returned from our api call.
                 * Then load campaign list into recyclerview.
                 */
                final List<Campaign> campaignList = response.body();
                mCampaignListAdapter.updateCampaignList(campaignList);

                /**
                 * When campaign list is added to adapter, notify adapter our data has changed.
                 * This will relayout the items in list and call onBindViewHolder method in CampaignListAdapter
                 */
                mCampaignListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Campaign>> call, Throwable t) {
                // Hide the progressbar when an error occurs.
                progressBar.setVisibility(View.GONE);

                // Show toast message to user indicating an has error occurred while fetching campaign list.
                Toast.makeText(MainActivity.this, R.string.error_fetch_campaign_list,
                        Toast.LENGTH_LONG).show();

                // Log error message for debugging purpose
                Log.e(TAG, getString(R.string.error_fetch_campaign_list), t);
            }
        });
    }

    @Override
    public void onItemClick(Campaign campaign) {
        // When a list item is clicked we will start detail activity here
        startActivity(DetailActivity.createIntent(this, campaign));
    }

    @Override
    protected void onDestroy() {
        /**
         * Remove OnItemClickListener from adapter so that we don't leak memory when an
         * activity gets destroyed.
         */
        mCampaignListAdapter.setOnItemClickListener(null);
        super.onDestroy();
    }
}