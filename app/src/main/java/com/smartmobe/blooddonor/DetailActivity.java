package com.smartmobe.blooddonor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * This class is used to show detail of campaign when clicked on a list item.
 * <p/>
 * Created by Ashish Kayastha on Aug 05.
 */
public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    // Default zoom level of MapView
    private static final float MAP_ZOOM_LEVEL = 14f;

    // Key which is used to store campaign model in intent extra
    private static final String EXTRA_CAMPAIGN = "campaign";

    // Campaign model passed from list activity when clicked on a list item
    private Campaign mCampaign;

    /**
     * This will create a new intent which will hold campaign model passed from list activity.
     * In Android, Intent is used to start activity, services, broadcast receivers.
     * In this scenario we will start detail activity from list activity.
     *
     * @param context  context to be used to start this activity
     * @param campaign campaign model which holds all data for particular list item
     * @return intent which will hold campaign model passed from list activity.
     */
    public static Intent createIntent(Context context, Campaign campaign) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_CAMPAIGN, campaign);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set this activity content to activity_detail.xml from a layout resource
        setContentView(R.layout.activity_detail);

        /**
         * Get our campaign model passed along with intent extras from list activity.
         * We will use this model to populate mapview and show other details in this activity.
         */
        mCampaign = getIntent().getParcelableExtra(EXTRA_CAMPAIGN);

        // Initialize Toolbar and set toolbar as an app's actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Add home button(left pointing arrow) on toolbar to navigate back to list activity
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // Initialize all textviews of detail activity
        TextView campaignNameText = (TextView) findViewById(R.id.text_campaign_name);
        TextView campaignDescText = (TextView) findViewById(R.id.text_campaign_desc);
        TextView campaignAddressText = (TextView) findViewById(R.id.text_campaign_address);
        TextView campaignDateTimeText = (TextView) findViewById(R.id.text_campaign_date_time);

        /**
         * Campaign can be null. Data may not be passed properly from list activity
         * or when device is rotated, campaign model is not saved in our case.
         * So to avoid NullPointerExeption(NPE) we need to check for null before using Campaign model.
         */
        if (mCampaign != null) {
            campaignNameText.setText(mCampaign.getCampaignName());
            campaignAddressText.setText(mCampaign.getAddress());
            campaignDescText.setText(mCampaign.getCampaignDescription());
            campaignDateTimeText.setText(mCampaign.getCampaignDateTime());
        }

        // Initialize Map Fragment from xml
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_fragment);

        /**
         * Sets a callback object which will be triggered when the GoogleMap
         * instance is ready to be used. When GoogleMap is ready to be used this will trigger
         * onMapReady() method which is implemented by this activity.
         */
        mapFragment.getMapAsync(this);
    }

    /**
     * This method is triggered when GoogleMap is ready to be used. From here we can work on MapView.
     *
     * @param googleMap ready to be used GoogleMap instance
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Check for null, before using Campaign model as described above.
        if (mCampaign != null) {
            /**
             * Get LatLng point of our campaign.
             * LatLng object is composed as (latitude, longitude)
             */
            final LatLng latLng = mCampaign.getLatLng();

            /**
             * Create marker options with position of marker set as campaign latlng and title as
             * campaign name. This will create default type marker.
             */
            final String campaignName = mCampaign.getCampaignName();
            final MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(campaignName);

            // Add marker to google map with marker options created above
            googleMap.addMarker(markerOptions);

            /**
             * This will update map camera with animation from default position of map(latlng -> (0.0, 0.0))
             * to the latlng position of particular campaign.
             */
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM_LEVEL);
            googleMap.animateCamera(update);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /**
             * We want home button shown in toolbar to act same as device back button
             * so we call onBackPressed() method when home button is pressed.
             * This will finish this activity and resume list activity.
             */
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}