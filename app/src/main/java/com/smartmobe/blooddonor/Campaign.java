package com.smartmobe.blooddonor;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

/**
 * This represents model of data that is returned by REST Api
 * <p/>
 * Created by Ashish Kayastha on Aug 02.
 */
public final class Campaign implements Parcelable {

    public static final Creator<Campaign> CREATOR = new Creator<Campaign>() {
        @Override
        public Campaign createFromParcel(Parcel in) {
            return new Campaign(in);
        }

        @Override
        public Campaign[] newArray(int size) {
            return new Campaign[size];
        }
    };

    @SerializedName("campaign_name")
    private String mCampaignName;

    @SerializedName("campaign_description")
    private String mCampaignDescription;

    @SerializedName("address")
    private String mAddress;

    @SerializedName("latitude")
    private double mLatitude;

    @SerializedName("longitude")
    private double mLongitude;

    @SerializedName("campaign_date_time")
    private String mCampaignDateTime;

    protected Campaign(Parcel in) {
        mCampaignName = in.readString();
        mCampaignDescription = in.readString();
        mAddress = in.readString();
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
        mCampaignDateTime = in.readString();
    }

    public String getCampaignName() {
        return mCampaignName;
    }

    public String getCampaignDescription() {
        return mCampaignDescription;
    }

    public String getAddress() {
        return mAddress;
    }

    public LatLng getLatLng() {
        return new LatLng(mLatitude, mLongitude);
    }

    public String getCampaignDateTime() {
        return mCampaignDateTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mCampaignName);
        parcel.writeString(mCampaignDescription);
        parcel.writeString(mAddress);
        parcel.writeDouble(mLatitude);
        parcel.writeDouble(mLongitude);
        parcel.writeString(mCampaignDateTime);
    }
}