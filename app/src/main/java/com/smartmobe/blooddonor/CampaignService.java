package com.smartmobe.blooddonor;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Ashish Kayastha on Aug 02.
 */
public interface CampaignService {

    @GET("campaign.json")
    Call<List<Campaign>> getCampaignList();
}