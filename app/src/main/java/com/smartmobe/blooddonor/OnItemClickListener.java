package com.smartmobe.blooddonor;

/**
 * This is an interface which handles click event on Recyclerview item.
 * Whenever a list item is clicked it triggers onItemClick method which is used to start detail activity,
 * <p>
 * Created by Ashish Kayastha on Aug 05.
 */
public interface OnItemClickListener {

    void onItemClick(Campaign campaign);
}